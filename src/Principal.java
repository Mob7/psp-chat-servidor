import java.io.IOException;
import java.util.Scanner;

/**
 * Created by sergio on 05/03/2016.
 */
public class Principal {

    public static void main(String args[]) {

        int puerto = 5555;

        Servidor servidor = new Servidor(puerto);
        Cliente cliente = null;

        try {
            servidor.conectar();
            while (servidor.estaConectado()) {
                cliente = new Cliente(servidor.escuchar(), servidor);
                cliente.start();
                System.out.println("Nuevo cliente conectado");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}

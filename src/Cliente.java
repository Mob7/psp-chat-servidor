import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by sergio on 05/03/2016.
 */
public class Cliente extends Thread {


    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private Servidor servidor;
    private String nick;
    private String ip;


    public Cliente(Socket socket, Servidor servidor) throws IOException {
        this.socket = socket;
        this.servidor = servidor;

        salida = new PrintWriter(socket.getOutputStream(), true);
        entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public PrintWriter getSalida() {
        return salida;
    }

    public void cambiarNick(String nick) {
        this.nick = nick;
    }

    public String getNick() {
        return nick;
    }

    @Override
    public void run() {
        boolean encontrado = false;
        System.out.println("Iniciando comunicación con el cliente");
        ip = socket.getInetAddress().getHostName();
        for (int i = 0; i < servidor.getListaIps().size(); i++) {
            if (servidor.getListaIps().get(i).equals(ip)) {
                encontrado = true;
            }
        }
        servidor.anadirIp(this);
        if (encontrado) {
            salida.println("/server Serás desconectado, tu IP está repetida");
            salida.println("/quit");
        } else {
            salida.println("/server Bienvenido. Tu IP quedará registrada como " + socket.getInetAddress().getHostName());
            salida.println("/server Introduce tu nick y un password y pulsa enter");
            try {
                String mensaje = entrada.readLine();
                String[] palabras = mensaje.split(" ");
                String nick = palabras[0];
                do{
                    salida.println("/server Error, no has introducido usuario y contraseña. Comprueba e introduce de nuevo ambos datos");
                    mensaje = entrada.readLine();
                    palabras = mensaje.split(" ");
                    nick = palabras[0];
                } while((palabras.length!=2));
                cambiarNick(nick);
                servidor.registrarCliente(nick, palabras[1], this);
                do {
                    if (servidor.isRegistrado() == false) {
                        mensaje = entrada.readLine();
                        palabras = mensaje.split(" ");
                        nick = palabras[0];
                        cambiarNick(nick);
                        servidor.registrarCliente(nick, palabras[1], this);
                    }
                } while (servidor.isRegistrado() == false);
                servidor.anadirCliente(this);
                salida.println("/server Bienvenido " + nick);
                salida.println("/sobrenombre "+nick);
                salida.println("/server Hay " + servidor.getNumeroClientes() + " usuarios conectados");
                salida.println("/server Cuando escribas '/quit', se terminará la conexión");
                salida.println("/server Escribe '/private' seguido del nombre de usuario para enviarle un mensaje privado");

                if (servidor.isRepetido() == true) {
                    salida.println("/server ¡Espera! Te has logeado dos veces con el mismo usuario, vas a ser expulsado automáticamente");
                    salida.println("/server Esto no te pasará si antes de logearte te deslogeas");
                    salida.println("/quit");
                    socket.close();
                }
                servidor.enviarNicks(this);

                String linea = null;
                while ((linea = entrada.readLine()) != null) {

                    if (linea.equals("/quit")) {
                        salida.println("/server Saliendo . . .");
                        socket.close();
                        servidor.eliminarCliente(this);
                        break;
                    }

                    if (linea.startsWith("/ban")) {
                        String[] comandos = linea.split(" ");
                        boolean existe = servidor.existe(comandos[1]);
                        if(existe==false){
                            salida.println("/server El usuario no existe. ¡Comprueba que exista antes de banearlo!");
                        } else {
                            System.out.print(comandos[1] + " baneado");
                            salida.println("/ban " + comandos[1]);
                        }

                    } else if (linea.startsWith("/private")) {
                        String[] comandos = linea.split(" ");
                        int cantidaduno = comandos[0].length();
                        int cantidaddos = nick.length();
                        int cantidadtres = comandos[1].length();
                        int cantidadtotal = cantidaduno+cantidaddos+cantidadtres-2;
                        String indice = linea.substring(cantidadtotal);
                        System.out.println("/private " + nick + " " + comandos[1] + ": " + indice);
                        servidor.enviarATodos("/private " + nick + " " + comandos[1] + ": " + indice);
                    } else {
                        servidor.enviarATodos("/users " + nick + " " + linea);
                    }

                }

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public String getIp() {
        return ip;
    }
}

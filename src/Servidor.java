import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by sergio on 05/03/2016.
 */
public class Servidor {

    private int puerto;
    private ServerSocket socket;

    private ArrayList<Cliente> clientes;
    private ArrayList<String> listaBaneados;
    private ArrayList<String> listaIps;
    private ArrayList<Usuarios> listaUsuarios;
    private boolean registrado;
    private boolean repetido;

    public Servidor(int puerto) {
        this.puerto = puerto;
        clientes = new ArrayList<>();
        listaBaneados = new ArrayList<>();
        listaIps = new ArrayList<>();
        listaUsuarios = new ArrayList<>();
    }

    public void anadirCliente(Cliente cliente) {
        for (int i=0; i<clientes.size(); i++){
            if(clientes.get(i).getNick().equals(cliente.getNick())){
                return;
            }
        }
        clientes.add(cliente);
    }

    public void eliminarCliente(Cliente cliente) {
        clientes.remove(cliente);
        enviarNicks(cliente);
    }

    public void enviarATodos(String mensaje) {
        String[] comandos = mensaje.split(" ");

        for (Cliente cliente : clientes){
            cliente.getSalida().println(mensaje);
        }

    }

    public void enviarNicks(Cliente thiscliente) {

        for (Cliente cliente : clientes) {
            cliente.getSalida().println(obtenerNicks());
        }
        thiscliente.getSalida().println(obtenerNicks());
    }

    public String obtenerNicks() {

        String nicks = "/nicks,";
        for (Cliente cliente : clientes)
            nicks += cliente.getNick() + ",";

        return nicks;
    }

    public int getNumeroClientes() {
        return clientes.size();
    }

    public boolean estaConectado() {
        return !socket.isClosed();
    }

    public void conectar() throws IOException {
        socket = new ServerSocket(puerto);
    }

    public void desconectar() throws IOException {
        socket.close();
    }

    public Socket escuchar() throws IOException {
        return socket.accept();
    }

    public boolean comprobarCliente(Cliente cliente){
        boolean repetido = false;
        for(int i=0; i>listaIps.size(); i++){
            if (listaIps.get(i).equals(socket.getInetAddress().getHostName())) {
                System.out.println(listaIps.get(i));
                System.out.println(cliente.getIp());
                repetido=true;
            }
        }
        return repetido;
    }

    public void anadirIp(Cliente cliente){
        listaIps.add(cliente.getIp());
    }

    public void registrarCliente(String nick, String password, Cliente cliente){
        Usuarios usuario = new Usuarios();
        if(listaUsuarios.size()==0){
            usuario.setUsuario(nick);
            usuario.setContrasena(password);
            listaUsuarios.add(usuario);
            cliente.getSalida().println("/server Nuevo usuario registrado");
            registrado=true;
            repetido=false;
            return;
        }
        for(int i=0; i<listaUsuarios.size(); i++) {
            if (listaUsuarios.get(i).getUsuario().equals(nick)) {
                if (listaUsuarios.get(i).getContrasena().equals(password)) {
                    cliente.getSalida().println("/server Te has logeado como " + nick);
                    registrado = true;
                    repetido=true;
                    return;
                } else {
                    cliente.getSalida().println("/server La contraseña no coincide");
                    registrado = false;
                    return;
                }
            }
        }
        usuario.setUsuario(nick);
        usuario.setContrasena(password);
        listaUsuarios.add(usuario);
        cliente.getSalida().println("/server Nuevo usuario registrado, enhorabuena");
        registrado=true;
        repetido=false;
        return;

    }

    public boolean isRegistrado() {
        return registrado;
    }

    public boolean isRepetido() {
        return repetido;
    }

    public boolean existe (String nick){
        boolean existe;
        existe=false;
        for(int i=0; i<listaUsuarios.size(); i++){
            if(listaUsuarios.get(i).getUsuario().equals(nick)){
                existe=true;
            }
        }
        return existe;
    }

    public ArrayList<String> getListaIps() {
        return listaIps;
    }

    //TODO No se puede conectar más de un cliente con el mismo equipo¡!
}

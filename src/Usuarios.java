/**
 * Created by sergio on 05/03/2016.
 */
public class Usuarios {

    private String usuario;
    private String contrasena;


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String toString(){
        return usuario;
    }
}
